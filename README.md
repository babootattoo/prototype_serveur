#TO IMPLEMENT

- JSON Processing after receiving JSON from client
- JSON Processing => CRUD Database
- Sending JSON to client

#ACTUAL BUILD

Structure :

 - View : Console with main function (call new thread of ServerSocket)
 - ConnectionPool (Model) : All database related classes
 - CommunicationServer (Controller) : All related socket classes and json processing

At start :
A new thread is started (it initializes the server socket)
Each time a client is reaching the server a new thread is created (this thread will treat the current socket => json processing)

An input functionnality is still available thanks to threads :
Different inputs :
- Close : close the server socket and terminate all the threads
(Propose different inputs which can be useful)