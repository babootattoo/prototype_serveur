package Model;

import connectionPool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * All database methods related to the Car class
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class CarDAO {
    
    /**
     * Create a car object by searching a car from the car table of the database with the specified id
     * @param id int Object
     * @return car object or null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static Car getCarById(int id) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        Car result = null;
        PreparedStatement stat = co.prepareStatement("SELECT matriculation, purchasedate, lastrevision "
                + "FROM car "
                + "WHERE id = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new Car(id, rs.getString("matriculation"), new DateFormatted(rs.getDate("purchasedate")), new DateFormatted(rs.getDate("lastrevision")));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Create a list of Car object by searching  all car which are identified in reparationForm with the cardState id at 1
     * @return result of Car object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     * @throws java.text.ParseException 
     */
    public static List<Car> getListBrokenCar() throws SQLException, ClassNotFoundException, InterruptedException, ParseException{
        List<Car> result = new ArrayList<Car>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT c.id AS id, c.matriculation AS matriculation, c.purchasedate AS purchasedate, c.lastrevision AS lastrevision "
                + "FROM car c "
                + "JOIN reparationform rf ON c.id = rf.idcar "
                + "JOIN cardstate cs ON cs.id = rf.idcardstate "
                + "WHERE cs.id = 1");
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Car(rs.getInt("id"), rs.getString("matriculation"), new DateFormatted(rs.getString("purchasedate")), new DateFormatted(rs.getString("lastrevision"))));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Update a car from the car table of the database
     * @param modifiedCar Car Object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static void updateCar(Car modifiedCar) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("UPDATE car SET "
                + "matriculation = '"+ modifiedCar.getMatriculation()+"',"
                + "purchaseDate = '"+ modifiedCar.getPurchaseDate()+"',"
                + "lastRevision = '"+ modifiedCar.getLastRevision()+"'"
                + " WHERE id = "+ modifiedCar.getId());
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }

    /**
     * Delete a car from the car table of the database
     * @param car Car object
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static void deleteCar(Car car) throws ClassNotFoundException, SQLException, InterruptedException{
        ReparationFormDAO.deleteRepForm(car);
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM car WHERE id = "+ car.getId());
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }
    
    /**
     * Create a car into the car table of the database,
     * And call createReparationForm to add vehicle into a new reparationForm.
     * @param matriculation
     * @param purchaseDate
     * @param lastRevision
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static void addCar(String matriculation, DateFormatted purchaseDate, DateFormatted lastRevision) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO Car(matriculation, purchaseDate, lastRevision) VALUES "
                + "('"+matriculation+"', '"+purchaseDate+"', '"+lastRevision+"');");
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
        ReparationFormDAO.createReparationForm();
    }
}
