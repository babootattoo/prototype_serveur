/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * All database methods related to the Parking class
 * @author Stephane.Schenkel
 */
public class ParkingDAO {
    
    /**
     * Create a Parking object by searching the Parking information in the database according to the specified id
     * @param id
     * @return a parking object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static Parking getParkingById(int id) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        Parking result = null;
        PreparedStatement stat = co.prepareStatement("SELECT occupied "
                + "FROM parking "
                + "WHERE id = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new Parking(id, rs.getBoolean("occupied"));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
