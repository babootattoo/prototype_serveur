/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * All database methods related to the User class
 * @author Stephane.Schenkel
 */
public class UserDAO {
    
    /**
     * Return a User object created from the database
     * It searches the user that has the corresponding login and password
     * If this user is not found in the database, it returns a null object
     * @param login
     * @param password
     * @return a user object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static User getUserByLoginPassword(String login, String password) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        User result = null;
        PreparedStatement stat = co.prepareStatement("SELECT users.id, firstname, lastname, address, town, postalcode, email, hiringdate, incomingperhour, workingtypeuser "
                + "FROM users, typeUser "
                + "WHERE login = ? AND password = ? AND idtypeuser = typeuser.id");
        stat.setString(1, login);
        stat.setString(2, password);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new User(rs.getInt("id"), rs.getString("workingtypeuser"), rs.getString("firstname"), rs.getString("lastname"), rs.getString("address"), rs.getString("town"), rs.getString("postalcode"), login, rs.getString("email"), new DateFormatted(rs.getDate("hiringdate")), rs.getFloat("incomingperhour"));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
