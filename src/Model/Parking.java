/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 * Parking class corresponding to the parking table of the database
 * @author Stephane.Schenkel, Charles.Santerre
 */
public class Parking {
    private int id;
    private boolean occupied;

    public Parking(int id, boolean occupied) {
        this.id = id;
        this.occupied = occupied;
    }
    
    @Override
    public String toString() {
        return "Parking{" + "id=" + id + ", occupied=" + occupied + '}';
    }

    public Parking(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
    
    public static String serialize(Parking parking){
        if(parking != null){
            return parking.id + "///" + parking.occupied;
        }else{
            return "null";
        }
    }
    
    public static Parking deserialize(String parking){
        if(parking.equals("null")){
            return null;
        }else{
            String[] eachAttributes = parking.split("///");
            return new Parking(Integer.parseInt(eachAttributes[0]), Boolean.parseBoolean(eachAttributes[1]));
        }
    }
    
}
