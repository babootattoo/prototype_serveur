package Model;

import connectionPool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * All database methods related to the ReparationForm class
 * @author Stephane.Schenkel, Brice.Boutamdja, Charles.Santerre
 */
public class ReparationFormDAO {
    
    /**
     * Get a ReparationForm by id from ReparationForm table of the database
     * It creates the Car object or Bike object of the ReparationForm and the Parking Object
     * @param id
     * @return a ReparationForm object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static ReparationForm getReparationFormById(int id) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        ReparationForm result = null;
        PreparedStatement stat = co.prepareStatement("SELECT rf.idcar, rf.idbike, rf.idparking, rf.entrydate, rf.outdate, rf.diagnosis, rf.reparationdetail, cs.description as descriptionCardState, ud.description as descriptionUrgencyDegree "
                + "FROM reparationform rf, urgencyDegree ud, cardState cs "
                + "WHERE rf.id = ? AND rf.idCardState = cs.id AND rf.idUrgencyDegree = ud.id");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        boolean found = rs.next();
        if (found) {
            Car car = null;
            int idCar = rs.getInt("idcar");
            if(!rs.wasNull()){
                car = CarDAO.getCarById(idCar);
            }
            Bike bike = null;
            int idBike = rs.getInt("idbike");
            if(!rs.wasNull()){
                bike = BikeDAO.getBikeById(idBike);
            }
            
            result = new ReparationForm(id, rs.getString("descriptionCardState"), rs.getString("descriptionUrgencyDegree"), ParkingDAO.getParkingById(rs.getInt("idparking")), car, bike, new DateFormatted(rs.getDate("entrydate")), new DateFormatted(rs.getDate("outdate")), rs.getString("diagnosis"), rs.getString("reparationdetail"));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * delete a repearationForm by idCar from ReparationForm table of the database
     * @param car
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    public static void deleteRepForm(Car car) throws ClassNotFoundException, SQLException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("DELETE FROM reparationForm WHERE idCar = "+ car.getId());
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }
    
    /**
     * Create ReparationForm a new reparationForm with the new car
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static  void createReparationForm() throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("INSERT INTO ReparationForm(idCardState, idUrgencyDegree, idParking, idCar, idBike, entryDate) VALUES"
                + "(1,1,1,(SELECT MAX(id) FROM car),null,'09-01-2017')");
        stat.executeUpdate();
        stat.close();
        Datasource.returnConnection(co);
    }
}
