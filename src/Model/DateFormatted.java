/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Personalized Date class, with a format of yyyy-MM-dd
 * @author Stephane.Schenkel
 */
public class DateFormatted{
    Date date;
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    
    public DateFormatted(String date) throws ParseException{
        this.date = format.parse(date);
    }
    
    public DateFormatted(Date date){
        this.date = date;
    }
    
    public String toString(){
        if(date != null){
            return format.format(date);
        }else{
            return null;
        }
    }
    
    public Date getDate(){
        return this.date;
    }
    
    public void setDate(Date date){
        this.date = date;
    }
    
    public void setDate(String date) throws ParseException{
        this.date = format.parse(date);
    }
}
