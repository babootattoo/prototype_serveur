/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * All database methods related to the Bike class
 * @author Stephane.Schenkel
 */
public class BikeDAO {
    
    /**
     * Create a bike object by searching a bike from the bike table of the database with the specified id
     * @param id
     * @return a bike object or a null object
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException 
     */
    public static Bike getBikeById(int id) throws SQLException, ClassNotFoundException, InterruptedException{
        Connection co = Datasource.getConnection();
        Bike result = null;
        PreparedStatement stat = co.prepareStatement("SELECT purchasedate, lastrevision "
                + "FROM bike "
                + "WHERE id = ?");
        stat.setInt(1, id);
        ResultSet rs = stat.executeQuery();
        if (rs.next()) {
            result = new Bike(id, rs.getDate("purchasedate").toString(), rs.getDate("lastrevision").toString());
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
    /**
     * Get all bike from the database that are on a reparationform, and the reparation needs to be in a state of "waiting reparation"
     * @return a list of bike object or an empty list
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws InterruptedException 
     */
    public static List<Bike> getListBrokenBike() throws SQLException, ClassNotFoundException, InterruptedException, InterruptedException{
        List<Bike> result = new ArrayList<Bike>();
        Connection co = Datasource.getConnection();
        PreparedStatement stat = co.prepareStatement("SELECT b.id AS id, b.purchasedate AS purchasedate, b.lastrevision AS lastrevision "
                + "FROM bike b "
                + "JOIN reparationform rf ON b.id = rf.idbike "
                + "JOIN cardstate cs ON cs.id = rf.idcardstate "
                + "WHERE cs.id = 1");
        ResultSet rs = stat.executeQuery();
        while(rs.next()){
            result.add(new Bike(rs.getInt("id"), rs.getString("purchasedate"), rs.getString("lastrevision")));
        }
        rs.close();
        stat.close();
        Datasource.returnConnection(co);
        return result;
    }
    
}
