package connectionPool;

/**
 * This class contains all the information to connect to the PostGres database
 * And contains the number of connection of the connection pool
 * @author Pierre.Toker
 */
public class DbConf {
		  
    public String DB_NAME, DB_USER_NAME, DB_PASSWORD, DB_URL, DB_DRIVER ;  
    public Integer DB_PORT, DB_MAX_CONNECTIONS;

    public DbConf(){
            init();
    }

    private static DbConf configuration = new DbConf();

    public DbConf getInstance(){ return configuration; }

    private void init(){
            DB_USER_NAME = "pdsbdd";
            DB_PASSWORD = "pdsbdd";
            DB_PORT = 5432;
            DB_NAME = "bddprot";
            DB_URL = "jdbc:postgresql://localhost:" + DB_PORT + "/" + DB_NAME;
            DB_DRIVER = "org.postgresql.Driver";
            DB_MAX_CONNECTIONS = 2;
    }
}
