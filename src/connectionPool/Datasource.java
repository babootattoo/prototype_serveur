package connectionPool;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

/**
 * This class uses the connection pool to get/return/close connections of the
 * connection pool
 * @authors Pierre.Toker, Stephane.Schenkel
 */
public class Datasource {
	private static ConnectionPool pool;
         
        /**
         * Initialize the ConnectionPool object
         */
        public static void initializeDataSource(){
            pool = new ConnectionPool();
        }
	  
        /**
         * Get a connection from the connection pool object and return it
         * @return a connection object
         * @throws ClassNotFoundException
         * @throws SQLException
         * @throws InterruptedException 
         */
        public static Connection getConnection() throws ClassNotFoundException, SQLException, InterruptedException{
           Connection connection = pool.getConnectionFromPool();
           return connection;
        }
	  
        /**
         * Add the Connection to the connection pool
         * @param connection 
         */
        public static void returnConnection(Connection connection) {
           pool.returnConnectionToPool(connection);
        }
         
        /**
         * Close every connection of the connection pool, it purges the connection pool.
         */
        public static void closeConnectionPool(){
           pool.closeAllConnection();
        }
        
        /**
         * Call the refresh_db() procedure of the database
         * Used for unit tests to have the same data
         * @throws ClassNotFoundException
         * @throws SQLException
         * @throws InterruptedException 
         */
        public static void resetDb() throws ClassNotFoundException, SQLException, InterruptedException{
            Connection co = Datasource.getConnection();
            Statement stat = co.createStatement();
            stat.executeQuery("SELECT refresh_db()");
            stat.close();
            Datasource.returnConnection(co);
        }
}
