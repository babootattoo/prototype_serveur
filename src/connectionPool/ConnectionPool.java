package connectionPool;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Vector;
import Model.ViewModel;

/**
 * Class of the connection pool
 * Contains a list of opened connection to the database
 * @author Pierre.Toker, Stephane.Schenkel
 */
public class ConnectionPool {

    /**
     * List of all the opened connection to the database.
     */
    Vector<Connection> availableConnections = new Vector<Connection>();
    /**
     * Configuration of the database.
     */
    DbConf uneConfiguration = new DbConf();

    /**
     * Initialize the connection pool.
     */
    public ConnectionPool() {
        initializeConnectionPool();
    }

    /**
     * Fill the List availableConnections with opened connection until the size of the List
     * is equal to the DB_MAX_CONNECTIONS attribute of the DbConf object.
     */
    private void initializeConnectionPool() {
        ViewModel.println("Initializing connection pool to " + uneConfiguration.DB_URL);
        while (!checkIfConnectionPoolIsFull()) {
            availableConnections.add(createNewConnectionForPool());
        }
        ViewModel.println("Connection pool initialized : " + availableConnections.size() + " available connections");
    }

    /**
     * This method check if the availableConnections size is lower than
     * the DB_MAX_CONNECTIONS attribute of the DbConf object.
     * @return false if lower, true if not lower
     */
    private synchronized boolean checkIfConnectionPoolIsFull() {
        final int MAX_POOL_SIZE = uneConfiguration.getInstance().DB_MAX_CONNECTIONS;

        if (availableConnections.size() < MAX_POOL_SIZE) {
            return false;
        }
        return true;
    }

    /**
     * Create a connection to the database according to the DbConf object
     * The connection is opened.
     * @return the created connection
     */
    private Connection createNewConnectionForPool() {
        DbConf config = uneConfiguration.getInstance();
        try {
            Class.forName(config.DB_DRIVER);
            Connection connection = DriverManager.getConnection(config.DB_URL, config.DB_USER_NAME, config.DB_PASSWORD);
            return connection;
        } catch (Exception e){
            ViewModel.printError(e);
        }
        return null;
    }

    /**
     * Return a connection and remove it from the availableConnections List
     * If availableConnections is empty, it waits a notify (the notify is triggered
     * when a connection is returned to the availableConnections list).
     * @return an opened connection to the database
     * @throws InterruptedException 
     */
    public synchronized Connection getConnectionFromPool() throws InterruptedException {
        Connection connection;
        if(availableConnections.isEmpty()){
            ViewModel.println("Connection pool is empty, thread waiting for a connection to return.");
            this.wait();
            ViewModel.println("A connection has returned, thread now running.");
        }
        connection = availableConnections.get(0);
        availableConnections.remove(0);
        return connection;
    }

    /**
     * Add the connection to the availableConnections List
     * If availableConnections was empty, it notifies and if a thread was waiting
     * to get a connection, it will be notified.
     * @param connection 
     */
    public synchronized void returnConnectionToPool(Connection connection) {
        if(availableConnections.isEmpty()){
            ViewModel.println("Connection pool was empty, returning connection and notifying other threads.");
            availableConnections.add(connection);
            this.notify();
        }else{
            availableConnections.add(connection);
        }
    }
    
    /**
     * Closes all the connection of the availableConnections List
     * Remove it from the List if the connection successfully closed
     * Loop until the availableConnections is empty.
     */
    public void closeAllConnection(){
        ViewModel.println("Closing all connections of the connection pool.");
        while(!availableConnections.isEmpty()){
            try{
                Connection co = availableConnections.firstElement();
                co.close();
                if(co.isClosed()){
                    availableConnections.remove(co);
                }
            }catch(SQLException e){
                ViewModel.printError(e);
            }
        }
        if(availableConnections.isEmpty()){
            ViewModel.println("All connections of the connection pool are closed.");
        }
    }
}
