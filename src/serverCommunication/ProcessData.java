package serverCommunication;

import Model.Bike;
import Model.BikeDAO;
import Model.Car;
import Model.CarDAO;
import Model.DateFormatted;
import Model.ReparationForm;
import Model.ReparationFormDAO;
import Model.ViewModel;
import Model.User;
import Model.UserDAO;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

/**
 * This class do a process depending on the input string of the Client The input
 * string should be a JsonObject containing a "Action" jsonvalue
 *
 * @author Stephane.Schenkel, Charles.Santerre, Brice.Boutamdja
 */
public class ProcessData {

    JsonObject input;
    JsonObject output;

    JsonObjectBuilder builder;

    String ipClient;

    /**
     * Set all the attributes and initialize the JsonObjectBuilder, this builder
     * will build the outputJson
     *
     * @param jsonString input string of the client, should be a JsonObject
     * @param ip ip of the client
     */
    public ProcessData(String jsonString, String ip) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        input = reader.readObject();
        builder = Json.createObjectBuilder();
        ipClient = ip;
        ViewModel.println("(" + ipClient + ") Processing request (Action : " + input.getString("Action") + ")");
    }

    /**
     * Do a certain process depending on the Action JsonValue of the input
     * JsonObject
     *
     * @return output jsonObject which will be sent to the client
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    public JsonObject getResult() throws SQLException, IOException, ClassNotFoundException, InterruptedException, ParseException {
        switch (input.getString("Action")) {
            case "Connection":
                builder.add("Action", input.getString("Action"));
                handlingConnection();
                break;
            case "giveListVehicleReparation":
                builder.add("Action", input.getString("Action"));
                handlingGetListBrokenVehicle();
                break;
            case "giveReparationForm":
                builder.add("Action", input.getString("Action"));
                giveReparationForm();
                break;
            case "updateVehicleReparation":
                builder.add("Action", input.getString("Action"));
                handlingUpdateVehicleReparation();
                break;
            case "deleteVehicleReparation":
                builder.add("Action", input.getString("Action"));
                handlingDeleteVehicleReparation();
                break;
            case "addVehicleReparation":
                builder.add("Action", input.getString("Action"));
                handlingAddVehicleReparation();
                break;
            default:
                builder.add("Action", "None");
                builder.add("State", "Action not found");
                break;
        }
        output = builder.build();
        ViewModel.println("(" + ipClient + ") Getting response (Action : " + output.getString("Action") + " AND State : " + output.getString("State") + ")");
        return output;
    }

    /**
     * Method which handle the action "Connection" It gets a User object within
     * the UserDAO with the corresponding Login and Password If the returned
     * User object is null, no user has been found on the database
     *
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void handlingConnection() throws SQLException, IOException, ClassNotFoundException, InterruptedException {
        User userLogged = UserDAO.getUserByLoginPassword(input.getString("Login"), input.getString("Password"));
        if (userLogged != null) {
            builder.add("State", "Success");
            builder.add("UserLogged", User.serialize(userLogged));
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No user found with this login and password");
        }
    }

    /**
     * Method which handle the action "giveReparationForm" It gets a
     * ReparationForm object from the ReparationFormDAO with the corresponding
     * id Add an ErrorMessage to the builder if no reparationform found
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    private void giveReparationForm() throws SQLException, ClassNotFoundException, InterruptedException {
        ReparationForm repForm = ReparationFormDAO.getReparationFormById(input.getInt("IdReparationForm"));
        if (repForm != null) {
            builder.add("State", "Success");
            Map<String, String> map = ReparationForm.serializeAll(repForm);
            builder.add("ReparationForm", map.get("ReparationForm"));
            builder.add("Car", map.get("Car"));
            builder.add("Bike", map.get("Bike"));
            builder.add("Parking", map.get("Parking"));
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No reparation form found");
        }
    }

    /**
     * Method which handle the action "GetListBrokenVehicle" It gets all Car and
     * Bike object which are in reparationForm Add an ErrorMessage to the
     * builder if no vehicle found
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     * @throws ParseException
     */
    private void handlingGetListBrokenVehicle() throws SQLException, ClassNotFoundException, InterruptedException, ParseException {
        List<Car> listCar = CarDAO.getListBrokenCar();
        List<Bike> listBike = BikeDAO.getListBrokenBike();
        if (!listCar.isEmpty() || !listBike.isEmpty()) {
            builder.add("State", "Success");
            if (!listCar.isEmpty()) {
                JsonArrayBuilder arrayBuilderCar = Json.createArrayBuilder();
                JsonObjectBuilder builderCar = Json.createObjectBuilder();
                int i = 0;
                for (Car car : listCar) {
                    builderCar.add(String.valueOf("Car" + i), Car.serialize(car));
                    arrayBuilderCar.add(builderCar);
                    i++;
                }
                builder.add("ListCar", arrayBuilderCar);
            } else {
                builder.add("ListCar", "null");
            }
            if (!listBike.isEmpty()) {
                JsonArrayBuilder arrayBuilderBike = Json.createArrayBuilder();
                JsonObjectBuilder builderBike = Json.createObjectBuilder();
                for (Bike bike : listBike) {
                    builderBike.add(String.valueOf(bike.getId()), Bike.serialize(bike));
                }
                arrayBuilderBike.add(builderBike);
                builder.add("ListBike", arrayBuilderBike);
            } else {
                builder.add("ListBike", "null");
            }

        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "No vehicle found");
        }
    }

    /**
     * Method which handle the action "updateVehicleReparation" It updates a car into the car table
     * of the database
     * the CarDAO calling updateCar (with a car object in parameter) to execute the SQL query
     * @throws ParseException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingUpdateVehicleReparation() throws ParseException, ClassNotFoundException, SQLException, InterruptedException {
        Car modifiedCar = Car.deserialize(input.getString("Car"));
        if (modifiedCar != null) {
            CarDAO.updateCar(modifiedCar);
            builder.add("State", "Success");
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Error in update");
        }
    }

    /**
     * Method which handle the action "deleteVehicleReparation" It deletes a car into the car table
     * of the database but it deletes all reparationForm which have the car
     * the CarDAO calling deleteCar (with a car object in parameter) to execute the SQL query
     * @throws ParseException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingDeleteVehicleReparation() throws ParseException, ClassNotFoundException, SQLException, InterruptedException {
        Car deleteCar = Car.deserialize(input.getString("Car"));
        if (deleteCar != null) {
            CarDAO.deleteCar(deleteCar);
            builder.add("State", "Success");
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Error in delete");
        }
    }
    
    /**
     * Method which handle the action "addVehicleReparation" It adds a car into the car table
     * of the database and it create a reparationForm after the crreation of car
     * the CarDAO calling addCar (with a all parameters needed) to execute the SQL query
     * @throws ParseException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InterruptedException 
     */
    private void handlingAddVehicleReparation() throws ParseException, ClassNotFoundException, SQLException, InterruptedException{
        String matriculation = input.getString("Matriculation");
        DateFormatted purchaseDate = new DateFormatted(input.getString("PurchaseDate"));
        DateFormatted lastRevision = new DateFormatted(input.getString("LastRevision"));
        if (matriculation != null && purchaseDate != null && lastRevision != null) {
            CarDAO.addCar(matriculation, purchaseDate, lastRevision);
            builder.add("State", "Success");
        } else {
            builder.add("State", "Error");
            builder.add("ErrorMessage", "Error in add");
        }
    }
}
