package serverCommunication;

import Model.ViewModel;
import connectionPool.ConnectionPool;
import connectionPool.Datasource;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Server class contains the ServerSocket
 * It extends Thread to not freeze the main thread during a loop in the Server class
 * @author Stephane.Schenkel
 */
public class Server extends Thread{
    
    private ServerSocket sc;
    private final ServConf conf = new ServConf();
    
    /**
     * Initialize the serversocket object with the port of the configuration class
     * Initialize the connection pool
     * @throws Exception 
     */
    public Server() throws Exception{
        sc = new ServerSocket(conf.PORT);
        Datasource.initializeDataSource();
        ViewModel.println("Listening to port " + conf.PORT + "\n");
    }
    
    /**
     * Contains a loop true, it opens a new Socket between a client and the server
     * each time a client wants to reach the server.
     * The socket is sent to the class SocketHandler, 
     * The new object SocketHandler is a Thread to be able to have multiple client
     * to communicate with the server at the same time.
     */
    @Override
    public void run(){
        try{
            while(true){
                Socket s = sc.accept();
                
                new Thread(new SocketHandler(s)).start();
            }
        }
        catch(SocketException e){
            Thread.currentThread().interrupt();
        }
        catch(IOException e){
            ViewModel.printError(e);
        }
    }
    
    /**
     * Closes the serversocket (it closes all opened client socket too)
     * Closes all the connection of the connection pool and purge the connection pool
     */
    public void closeServer(){
        try{
            ViewModel.println("Closing server socket...");
            sc.close();
            if(sc.isClosed()){
                ViewModel.println("Server socket is closed");
            }
            Datasource.closeConnectionPool();
        }catch(IOException e){
            ViewModel.printError(e);
        }
    }
}
