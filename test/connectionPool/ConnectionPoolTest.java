/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectionPool;

import Model.ViewModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Brice.Boutamdja, Stephane.Schenkel
 */
public class ConnectionPoolTest {
    
    ConnectionPool con = null;
    
    @Before
    public void initializeCon(){
        con = new ConnectionPool();
    }

    @Test
    public void testinitializeConnectionPool() throws Exception {
        if(!con.availableConnections.isEmpty()){
            assertEquals(con.availableConnections.firstElement().isValid(5), true);
        }else{
            fail("No connections in the pool");
        }
    }
    
    @Test
    public void testGetConnectionFromPool() throws Exception {
        System.out.println("getConnectionFromPool");
        Connection expResult = con.availableConnections.firstElement();
        Connection result = con.getConnectionFromPool();
        assertEquals(expResult, result);

    }

    @Test
    public void testReturnConnectionToPool() throws InterruptedException {
        System.out.println("returnConnectionToPool");
        Connection co = con.getConnectionFromPool();
        int size = con.availableConnections.size();
        con.returnConnectionToPool(co);
        size++;
        assertEquals(size, con.availableConnections.size());
    }

    @Test
    public void testCloseAllConnection() {
        System.out.println("closeAllConnection");
        con.closeAllConnection();
        int size = con.availableConnections.size();
        assertEquals(0, size);
    }
    
    @Test
    public void testWaitNotify() throws Exception{
        System.out.println("testWaitNotify");
        Thread test1 = new Thread(){ 
            public void run() {
                try{
                    Connection co = con.getConnectionFromPool(); 
                    this.sleep(5000);
                    con.returnConnectionToPool(co);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        };
        Thread test2 = new Thread(){ 
            public void run() {
                try{
                    Connection co = con.getConnectionFromPool(); 
                    this.sleep(5000);
                    con.returnConnectionToPool(co);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        };
        Thread test3 = new Thread(){ 
            public void run() {
                try{
                    Connection co = con.getConnectionFromPool(); 
                    this.sleep(5000);
                    con.returnConnectionToPool(co);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        };
        test1.start();
        test2.start();
        test3.start();
        test3.join();
    }
    
    @After
    public void closeCon(){
        con.closeAllConnection();
    }
    
}
