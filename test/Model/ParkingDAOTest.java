/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja, Stephane.Schenkel
 */
public class ParkingDAOTest {
    
    @Before
    public void initialize() {
        Datasource.initializeDataSource();
    }
    
    /**
     * Test of getParkingById method, of class ParkingDAO.
     */
    @Test
    public void testGetParkingById() throws Exception {
        System.out.println("getParkingById");
        int id = 1;
        Parking result = ParkingDAO.getParkingById(id);
        Parking expResult = new Parking(1, true);
        assertEquals(expResult.toString(), result.toString());
    }
    
}
