/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja, Stephane.Schenkel
 */
public class ReparationFormDAOTest {

    @Before
    public void initialize() throws Exception{
        Datasource.initializeDataSource();
        Datasource.resetDb();
    }
    /**
     * Test of getReparationFormById method, of class ReparationFormDAO.
     */
    @Test
    public void testGetReparationFormById() throws Exception {
        int id = 1;
        ReparationForm result = ReparationFormDAO.getReparationFormById(id);
        Car car = CarDAO.getCarById(1);
        Parking park = ParkingDAO.getParkingById(1);
        ReparationForm expResult = new ReparationForm(1,"Waiting reparation","normal",park,car,null,new DateFormatted("2017-01-09"),new DateFormatted("2017-02-03"),null,null);
        assertEquals(expResult.toString(), result.toString());
    }
    
    @Test
    public void testDeleteRepForm() throws Exception{
        ReparationForm expResult = null;
        ReparationFormDAO.deleteRepForm(CarDAO.getCarById(4)); //It should delete the reparationform with id 2
        ReparationForm result = ReparationFormDAO.getReparationFormById(2);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateReparationForm() throws Exception{
        Car car = new Car(5, "69 IZI 69", new DateFormatted("2016-10-02"), new DateFormatted("2017-02-10"));
        Parking park = ParkingDAO.getParkingById(1);
        Date date = null;
        ReparationForm expResult = new ReparationForm(3,"Waiting reparation","normal",park,car,null,new DateFormatted("2017-01-09"), new DateFormatted(date),null,null);
        CarDAO.addCar("69 IZI 69", new DateFormatted("2016-10-02"), new DateFormatted("2017-02-10"));
        ReparationForm result = ReparationFormDAO.getReparationFormById(3);
        assertEquals(expResult.toString(), result.toString());
    }
    
    @After
    public void closeConnection() throws Exception{
        Datasource.resetDb();
        Datasource.closeConnectionPool();
    }
}
