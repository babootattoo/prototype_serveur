/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja, Stephane.Schenkel
 */
public class CarDAOTest {
    
    /**
     * Initialize connection pool and reset the occurences of the database.
     */
    @Before
    public void initialize() throws Exception{
        Datasource.initializeDataSource();
        Datasource.resetDb();
    }
    /**
     * Test of getCarById method, of class CarDAO.
     */
    @Test
    public void testGetCarById() throws Exception {
        System.out.println("testGetCarById");
        int id = 1;
        Car result = CarDAO.getCarById(id);
        Car expResult = new Car(1,"45 RFS 64",new DateFormatted("2016-12-02"), new DateFormatted("2017-02-07"));
        System.out.println(expResult.toString());
        System.out.println(result.toString());
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test of getListBrokenCar method, of class CarDAO.
     */
    @Test
    public void testGetListBrokenCar() throws Exception {
        System.out.println("testGetListBrokenCar");
        List<Car> expResult = new ArrayList<Car>();
        expResult.add(new Car(1,"45 RFS 64",new DateFormatted("2016-12-02"), new DateFormatted("2017-02-07")));
        expResult.add(new Car(4, "80 ABC 05", new DateFormatted("2016-10-29"), new DateFormatted("2017-01-06")));
        List<Car> result = CarDAO.getListBrokenCar();
        System.out.println(expResult.toString());
        System.out.println(result.toString());
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test of updateCar method, of class CarDAO.
     */
    @Test
    public void testUpdateCar() throws Exception{
        System.out.println("testUpdateCar");
        Car expResult = CarDAO.getCarById(1);
        expResult.setMatriculation("45 RFS 70");
        CarDAO.updateCar(expResult);
        Car result = CarDAO.getCarById(expResult.getId());
        System.out.println(expResult.toString());
        System.out.println(result.toString());
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Test of deleteCar method, of class CarDAO.
     */
    @Test
    public void testDeleteCar() throws Exception{
        System.out.println("testDeleteCar");
        Car expResult = null;
        Car result = CarDAO.getCarById(1);
        CarDAO.deleteCar(result);
        result = CarDAO.getCarById(1);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addCar method, of class CarDAO.
     */
    @Test
    public void testAddCar() throws Exception{
        System.out.println("testAddCar");
        Car expResult = new Car(5, "90 OUI 69", new DateFormatted("2016-11-30"), new DateFormatted("2017-02-03"));
        CarDAO.addCar("90 OUI 69", new DateFormatted("2016-11-30"), new DateFormatted("2017-02-03"));
        Car result = CarDAO.getCarById(5);
        System.out.println(expResult.toString());
        System.out.println(result.toString());
        assertEquals(expResult.toString(), result.toString());
    }
    
    /**
     * Closes all the connections of the connection pool and reset the occurences of the database.
     */
    @After
    public void closeConnection() throws Exception{
        Datasource.resetDb();
        Datasource.closeConnectionPool();
    }
}
