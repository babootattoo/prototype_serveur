/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import connectionPool.Datasource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author charles.santerre, Stephane.Schenkel
 */
public class UserDAOTest {
    
    @Before
    public void initializePool(){
        Datasource.initializeDataSource();
    }
    
    /**
     * Test of getUserByLoginPassword method, of class UserDAO.
     */
    @Test
    public void testGetUserByLoginPasswordSuccessful() throws Exception {
        String login = "csanterre";
        String password = "mdp";
        User expResult = new User(1,"reparateur","SANTERRE","Charles","82 rue des poulettes","Paris","75005","csanterre","csanterre@gmail.com",new DateFormatted("2016-11-05"), 7.5);
        User result = UserDAO.getUserByLoginPassword(login, password);
        assertEquals(expResult.toString(), result.toString());
    }
    
    @Test
    public void testGetUserByLoginPasswordFailed() throws Exception {
        String login = "csanterre";
        String password = "mdp2";
        User expResult = null;
        User result = UserDAO.getUserByLoginPassword(login, password);
        assertEquals(expResult, result);
    }
    
}
