/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverCommunication;

import java.io.DataOutputStream;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.net.Socket;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Stephane.Schenkel
 */
public class ServerTest {
    
    Server serv = null;
    
    @Before
    public void initializeServer() throws Exception{
        serv = new Server();
        serv.start();
    }
    
    @Test
    public void testOneConnection() throws Exception{
        Socket s = new Socket("localhost", 10080);
        boolean expResult = true;
        boolean result = s.isConnected();
        PrintWriter pw = new PrintWriter(new DataOutputStream(s.getOutputStream()), true);
        pw.println("END");
        pw.close();
        s.close();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testMultipleConnection() throws Exception{
        class coTest extends Thread{
            private int time;
            public coTest(int time){
                this.time = time;
            }
            public void run(){
                try{
                    Socket s = new Socket("localhost", 10080);
                    this.sleep(time);
                    PrintWriter pw = new PrintWriter(new DataOutputStream(s.getOutputStream()), true);
                    pw.println("END");
                    pw.close();
                    s.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
        coTest test1 = new coTest(5000);
        coTest test2 = new coTest(2000);
        coTest test3 = new coTest(3000);
        test1.start();
        test2.start();
        test3.start();
        test1.join();
        test2.join();
        test3.join();
    }
    
    @After
    public void closeServer() throws Exception{
        serv.closeServer();
    }
    
}
