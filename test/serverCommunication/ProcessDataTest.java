/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverCommunication;

import connectionPool.Datasource;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Brice.Boutamdja, Charles.Santerre
 */
public class ProcessDataTest {

    @Before
    public void initialize(){
        Datasource.initializeDataSource();
    }
    
    /**
     * Test of getResult method, of class ProcessData.
     */
    @Test
    public void testGetResult() throws Exception {
        System.out.println("getResult");
        JsonObjectBuilder builderAsk = Json.createObjectBuilder();
        builderAsk.add("Action", "Connection");
        builderAsk.add("ConnectionState", "Try");
        builderAsk.add("Login", "csanterre");
        builderAsk.add("Password", "mdp");
        JsonObject json = builderAsk.build();
        ProcessData instance = new ProcessData(json.toString(), "10.10.10.147");
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Connection");
        builder.add("State", "Success");
        builder.add("UserLogged", "1///reparateur///SANTERRE///Charles///82 rue des poulettes///Paris///75005///csanterre///csanterre@gmail.com///2016-11-05///7.5");
        JsonObject expResult = builder.build();
        JsonObject result = instance.getResult();
        assertEquals(expResult, result);
    }
    
}
