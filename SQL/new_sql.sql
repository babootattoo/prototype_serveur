alter table reparationForm drop constraint fk_reparationform_car;
create table bike (id int primary key, purchasedate date, lastrevision date);
alter table reparationform drop idvehicle;
alter table reparationform add idbike int references bike(id);
alter table reparationform add idcar int references car(id);
update reparationform set outdate = '2017-02-03' where id = 1;
update reparationform set idcar = 1 where id = 1;